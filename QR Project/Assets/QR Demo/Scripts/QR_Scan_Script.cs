﻿using UnityEngine;
using System.Collections;
using ZXing;
using ZXing.QrCode;
using System.IO;
using System;

public class QR_Scan_Script : MonoBehaviour
{
    private float screenX;
    private float screenY;
    WebCamTexture camera;
    int cameraheight, camerawidth;
    Color32[] colors;
    bool nextframe = false;
    bool gotCode = false;
    string Code;
    bool start = true;
    bool draw = false;
    string btnName;

    void Awake()
    {
        screenX = Screen.width;
        screenY = Screen.height;
    }
    void Start()
    {
        btnName = "Stop Scanning";
    }
    GUIStyle style;
    GUIStyle style1;
    GUIStyle style2;
  
    void Update()
    {
        BarcodeReader barcodeReader = new BarcodeReader { AutoRotate = false, TryHarder = false };
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (!draw && gotCode)
            {
                gotCode = false;
                draw = true;
                RenderSettings.skybox = null;


            }
            else if (draw && !gotCode)
            {
                draw = false;
                start = true;
                camera.Stop();
                camera = null;
            }
            else if (start && !draw)
            {
                Application.Quit();

            }

        }
        if (colors == null)
        {
            if (camera != null)
            {
                colors = camera.GetPixels32();
                nextframe = true;
            }
        }
        if (nextframe)
        {
            var result = barcodeReader.Decode(colors, camerawidth, cameraheight);
            if (result != null)
            {
                Code = result.Text;
                gotCode = true;
            }
            colors = null;
            nextframe = false;
        }

    }

    void SceneChange(Material sky)
    {
        start = false;
        draw = false;
        RenderSettings.skybox = sky;
        if (GUI.Button(new Rect(10, 20, 120, 30), "BACK"))
        {
            gotCode = false;
            draw = true;
            RenderSettings.skybox = null;

        }       
    }
    
    void OnGUI()
    {
        style = GUI.skin.GetStyle("TextArea");
        style.alignment = TextAnchor.MiddleCenter;
        style1 = GUI.skin.GetStyle("Button");
        style1.alignment = TextAnchor.MiddleCenter;
        style2 = GUI.skin.GetStyle("Label");
        style2.alignment = TextAnchor.MiddleCenter;
        style.fontSize = (int)(Screen.height * .06F);
        style1.fontSize = (int)(Screen.height * .06F);
        style2.fontSize = (int)(Screen.height * .06F);
        GUI.skin.box.alignment = TextAnchor.MiddleCenter;
        GUI.skin.box.font = Resources.Load("Neuropol") as Font;
        GUI.skin.box.normal.background = Resources.Load("grey") as Texture2D;
        if (start)
        {
            if (GUI.Button(new Rect(2 * screenX / 5 +20f, screenY - screenY / 12, screenX / 5, screenY / 12), "Start Scanning"))
            {
                camera = new WebCamTexture();
                camera.Play();
                if (camera != null)
                {

                    cameraheight = camera.height;
                    camerawidth = camera.width;
                }
                draw = true;
            }
        }
        if (draw)
        {
            GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), camera, ScaleMode.ScaleToFit);
            btnName = "Stop Scanning";
        }
        else
        {
            btnName = "Gen. QR Codes";
        }

        if (GUI.Button(new Rect(1 * screenX / 5 + 15f, screenY - screenY / 12, screenX / 5, screenY / 12), btnName))
        {
            if (draw)
            {
                draw = false;
                start = true;
                camera.Stop();
                camera = null;
            }
            else
            {
                Application.LoadLevel("QR_Gen");                
            }
        }
        if (GUI.Button(new Rect(3 * screenX / 5+30f, screenY - screenY / 12, screenX / 6, screenY / 12), "Exit"))
        {
            if (draw)
            {
                draw = false;
                start = true;
                camera.Stop();
                camera = null;
            }
           
            Application.Quit();
        }

        
        if (gotCode)
        {
            print("Scanned - " + Code);
            GUI.Label(new Rect(Screen.width * 0.1F, Screen.height * 0.05F, Screen.width * 0.8f, Screen.height * 0.1f), Code);
        }

        
    }
}
