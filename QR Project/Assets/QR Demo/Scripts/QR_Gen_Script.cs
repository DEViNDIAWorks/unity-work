﻿using UnityEngine;
using System.Collections;
using ZXing;
using ZXing.QrCode;


public class QR_Gen_Script : MonoBehaviour
{

	// Use this for initialization
	void Start () {
        texture1 = (Texture2D)Resources.Load("back");
	}
    Texture2D texture;
    Texture2D texture1;
    
	// Update is called once per frame
	void Update () {

        print(abc);
	}
    GUIStyle style;
    GUIStyle style1;
    GUIStyle style2;
  

    
    string abc="";
    private static Texture2D GetQrcode(string textForEncoding)
    {
        var writer = new BarcodeWriter
        {
            Format = BarcodeFormat.QR_CODE,
            Options = new QrCodeEncodingOptions
            {
                Height = 256,
                Width = 256
            }
        };

        Texture2D Qrcode = new Texture2D(256, 256);
        Qrcode.SetPixels32(writer.Write(textForEncoding));
        Qrcode.Apply();
        return Qrcode;
    }
    void OnGUI()
    {
        style = GUI.skin.GetStyle("TextArea");
        style.alignment = TextAnchor.MiddleCenter;
        style1 = GUI.skin.GetStyle("Button");
        style1.alignment = TextAnchor.MiddleCenter;
        style2 = GUI.skin.GetStyle("Label");
        style2.alignment = TextAnchor.MiddleCenter;
        style.fontSize = (int)(Screen.height * .06F);
        style1.fontSize = (int)(Screen.height * .06F);
        style2.fontSize = (int)(Screen.height * .06F);
     
        GUI.Label(new Rect(Screen.width * 0.1F, Screen.height * 0.05F, Screen.width * 0.8f, Screen.height * 0.1f), "QR Code Demo by DevIndia");
     
            // GUI.Box(new Rect(Screen.width * 0.1F, Screen.height * 0.15F, Screen.width * 0.8f, Screen.height * 0.6f), texture1);
             if (texture != null)
             {
                 GUI.Box(new Rect(Screen.width * 0.2F, Screen.height * 0.25F, Screen.width * 0.6f, Screen.height * 0.5f), texture);
             }
             GUI.Label(new Rect(-130.0F, Screen.height * 0.77F, Screen.width * 0.8f, Screen.height * 0.1f), "Enter Code");
       abc = GUI.TextArea(new Rect(Screen.width * 0.2F, Screen.height * 0.77F, Screen.width * 0.6f, Screen.height * 0.1f), abc);
      if (GUI.Button(new Rect(Screen.width * 0.20F, Screen.height *0.89F, Screen.width * 0.29f, Screen.height * 0.1f), "Generate QR Code"))
       {
            if (abc != "")
            {
               texture = GetQrcode(abc);      
                
           }
       }
      if (GUI.Button(new Rect(Screen.width * 0.51F, Screen.height * 0.89F, Screen.width * 0.29f, Screen.height * 0.1f), "Scan QR Code"))
      {
          Application.LoadLevel("QR_Scan");
      }
    }
}
