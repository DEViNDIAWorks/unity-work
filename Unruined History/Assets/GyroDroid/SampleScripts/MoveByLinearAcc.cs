using UnityEngine;
using System.Collections;

public class MoveByLinearAcc : MonoBehaviour {
	
	// Use this for initialization
	void Start () {
		Sensor.Activate(Sensor.Type.Accelerometer);
	}
	
	
	void FixedUpdate () {
		Vector3 linearAcc = FilterMax(Sensor.accelerometer / 20 * 350*Time.deltaTime);
		rigidbody.position = new Vector3( linearAcc.x, rigidbody.position.y, linearAcc.y);

       // rigidbody.AddForce(linearAcc.x, 0f, linearAcc.y);
	}
	
	Vector3 holder = Vector3.zero;
	Vector3 max = Vector3.zero;
	Vector3 velocity = Vector3.zero;
	
	Vector3 FilterMax(Vector3 input)
	{
		if(input.magnitude > max.magnitude) max = input; 
		   
		holder = Vector3.SmoothDamp(holder, max, ref velocity, 0.1f);
		if(Vector3.Distance(holder, max) < 0.4f) max = Vector3.zero;
		return holder;
	}
}