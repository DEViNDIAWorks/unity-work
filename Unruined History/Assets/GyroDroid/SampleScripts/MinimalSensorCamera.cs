// prefrontal cortex -- http://prefrontalcortex.de
// Full Android Sensor Access for Unity3D
// Contact:
// 		contact@prefrontalcortex.de

using UnityEngine;
using System.Collections;

public class MinimalSensorCamera : MonoBehaviour
{

    // Use this for initialization
    GameObject pointer;
    Camera[] cm;
    public static bool isRotate = false;
    public static bool isInitialize = true;

    void Start()
    {
        // you can use the API directly:
        // Sensor.Activate(Sensor.Type.RotationVector);

        // or you can use the SensorHelper, which has built-in fallback to less accurate but more common sensors:
        SensorHelper.ActivateRotation();
        useGUILayout = false;

        cm = Camera.allCameras;
        pointer = GameObject.Find("3D_Pointer");
    }

    // Update is called once per frame
    float lastPointX;
    float lastPointY;
    void Update()
    {
        
        if (isRotate)
        {
            transform.rotation = SensorHelper.rotation;
            isInitialize = false;
        }
        else
        {           
            transform.rotation = pointer.transform.rotation;
        }
        

        if (cm[0].active) //Check you are viewing MiniMap(skybox) or not.
        {
            if (isInitialize)
            {
                lastPointX = SensorHelper.rotation.x;
                lastPointY = SensorHelper.rotation.y;
                isInitialize = false;
            }
            if (SensorHelper.rotation.x > lastPointX + 0.188f)  // check sensor movement from last
            {
                isRotate = true;
            }

            if (SensorHelper.rotation.y > lastPointY + 0.058f)
            {
                isRotate = true;
            }

            if (SensorHelper.rotation.y < lastPointY - 0.088f)
            {
                isRotate = true;
            }
        }
        else
        {
            isRotate = false;
            isInitialize = false;
        }


    }
    void FixUpdate()
    {


    }
}