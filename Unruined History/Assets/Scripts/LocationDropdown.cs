﻿using UnityEngine;
using System.Collections;

public class LocationDropdown : MonoBehaviour {

	public string[] items;
	public string selectedItem = "--Choose Temple--";
	private bool editing = false;
	public Rect box;

   private void	OnGUI()
	{
		if (GUI.Button(box,selectedItem)) {
			editing =true;
				}
		if (editing) {
			for (int i = 0; i < items.Length; i++) {
				if (GUI.Button(new Rect(box.x,(box.height*i)+box.y +box.height,box.width,box.height),items[i])) {
					selectedItem = items[i];
					editing = false;
				}
			}
				}
    }
}
