﻿using UnityEngine;
using System.Collections;

public class welcome : MonoBehaviour
{
    public Transform initText;
    public static bool emulator = true;
    private Camera _camera;
    public static bool isOffiline = false;

    private float screenX;
    private float screenY;
    void Awake()
    {
        screenX = Screen.width;
        screenY = Screen.height;
        //  initText = GameObject.Find("GUIText");// gameObject.transform.Find("GUIText");

        //Set GUIText font size according to our device screen size
        initText.guiText.fontSize = (int)Mathf.Round(15 * Screen.width / 320);
    }
    void Start()
    {
        //Initialization message
        initText.guiText.text = "Please choose an option for load Map.";
        initText.guiText.color = Color.cyan;
        //Enable initial screen
        initText.gameObject.SetActive(true);
        _camera = GameObject.FindWithTag("MainCamera").camera;
    }
    
    private Color lerpedColor = Color.white;
    void Update()
    {
        lerpedColor = Color.Lerp(Color.white, Color.black, Time.time);
        _camera.backgroundColor = lerpedColor;

        if(Input.GetKey(KeyCode.Escape))
        {
            Application.Quit();
        }

    }
    
    /*void OnGUI()
    {
        label = GUI.skin.GetStyle("Label");
        label.fontSize = (int)Mathf.Round(10 * Screen.width / 320);
        label.font = Resources.Load("Neuropol") as Font;
        GUIStyle myButtonStyle = new GUIStyle(GUI.skin.button);
        myButtonStyle.fontSize = (int)Mathf.Round(10 * Screen.width / 320);

        if (GUI.Button(new Rect(Screen.width / 2 - 440, Screen.height - 100, 260, 60), "Offline", myButtonStyle))
        {
            isOffiline = true;
            emulator = true;
            draw = true;
            Application.LoadLevel("scn_Locations_offline");
        }
        if (GUI.Button(new Rect(Screen.width / 2 - 160, Screen.height - 100, 260, 60), "GPS On", myButtonStyle))
        {
            if (Input.location.isEnabledByUser == false) //
            {
                initText.guiText.text = "Please enable gps in your device.";
                initText.guiText.color = Color.red;
            }
            else
            {
                isOffiline = false;
                emulator = false;
                draw = true;
                Application.LoadLevel("scn_Locations");
            }

        }
        if (GUI.Button(new Rect(Screen.width / 2 + 110, Screen.height - 100, 260, 60), "GPS Off", myButtonStyle))
        {
            isOffiline = false;
            emulator = true;
            draw = true;
            Application.LoadLevel("scn_Locations");
        }
        if (draw)
        {
            GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), image);
            GUI.Label(new Rect((Screen.width / 2) - 130, Screen.height / 2, 260, 60), "Loading...");
        }
    } */

    public Texture2D image;
    bool draw = false;
    GUIStyle label;
    string abc = "Loading...";
    void OnGUI()
    {

        label = GUI.skin.GetStyle("Label");
        label.fontSize = (int)Mathf.Round(10 * Screen.width / 320);
        label.font = Resources.Load("Neuropol") as Font;
        if (draw)
        {
            GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), image);
            GUI.Label(new Rect((Screen.width / 2) - 130, Screen.height / 2, 600, 60), abc);
        }
        screenX = Screen.width;
        screenY = Screen.height;
        GUI.skin.box.alignment = TextAnchor.MiddleCenter;
        GUI.skin.box.font = Resources.Load("Neuropol") as Font;
        GUI.skin.box.normal.background = Resources.Load("grey") as Texture2D;
        if (Screen.width >= Screen.height)
        {
            GUI.skin.button.fontSize = (int)Mathf.Round(10 * Screen.width / 480);
            GUI.skin.box.fontSize = (int)Mathf.Round(10 * Screen.width / 320);
        }
        else
        {
            GUI.skin.button.fontSize = (int)Mathf.Round(10 * Screen.height / 480);
            GUI.skin.box.fontSize = (int)Mathf.Round(10 * Screen.height / 320);
        }

        //if (GUI.Button(new Rect(0, screenY - screenY / 12, screenX / 5, screenY / 12), "Go to Map"))
        //{
        //    UserOption.isMini_Map = false;
        //}
        if (GUI.Button(new Rect(screenX / 5, screenY - screenY / 12, screenX / 5, screenY / 12), "OFFLINE"))
        {
            isOffiline = true;
            emulator = true;
            draw = true;
            Application.LoadLevel("scn_Locations_offline");
        }
        if (GUI.RepeatButton(new Rect(2 * screenX / 5, screenY - screenY / 12, screenX / 5, screenY / 12), "GPS ON"))
        {
            if (Input.location.isEnabledByUser == false) //
            {
                initText.guiText.text = "Please enable gps in your device.";
                initText.guiText.color = Color.red;
            }
            else
            {
                isOffiline = false;
                emulator = false;
                draw = true;
                Application.LoadLevel("scn_Locations");
            }

        }



        //Update map and center user position 


        if (GUI.Button(new Rect(3 * screenX / 5, screenY - screenY / 12, screenX / 5, screenY / 12), "GPS OFF"))
        {
            isOffiline = false;
            emulator = true;
            draw = true;
            Application.LoadLevel("scn_Locations");
        }
        if (draw)
        {
            GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), image);
            GUI.Label(new Rect((Screen.width / 2) - 130, Screen.height / 2, 260, 60), "Loading...");
        }
        //Show GPS Status info. Please make sure the GPS_Status.cs script is attached and enabled in the map object.
        //if (GUI.Button(new Rect(4 * screenX / 5, screenY - screenY / 12, screenX / 5, screenY / 12), "Quit"))
        //{


        //    Application.Quit();
        //}
        //GUI.EndGroup ();


        //________________Navigation Nos_________________________________________



    }
}
