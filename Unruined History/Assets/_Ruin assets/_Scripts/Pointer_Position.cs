﻿using UnityEngine;
using System.Collections;

public class Pointer_Position : MonoBehaviour
{
    public GameObject pointer;
	// Use this for initialization
	void Start () 
    {
        transform.rotation = pointer.transform.rotation;
	}
	
	// Update is called once per frame
    void Update()
    {
        if ((Input.GetKey(KeyCode.LeftArrow) ||(Input.GetKey(KeyCode.RightArrow)) ))
        {            
            transform.rotation = pointer.transform.rotation;
        }
        else
        {
            Quaternion qt = new Quaternion(0f, transform.rotation.y, 0f, transform.rotation.w);
            pointer.transform.rotation = qt;
        }
    }
    void FixUpdate()
    {
      //  transform.rotation = pointer.transform.rotation;
    }
}
