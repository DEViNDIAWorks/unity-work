﻿using UnityEngine;
using System.Collections;

public class Location_Info : MonoBehaviour
{

    private Texture img;
    void Start()
    {
        if (locationMenu.loadLocation == "zurich")
        {
            img = Resources.Load("Images/Zurich1/pic1") as Texture;
           
        }
    }


    void Update()
    {

    }

    private Vector2 scrollViewVector = Vector2.zero;
    private string innerText = @"In 1199, Cistercian monks founded the Hilda monastery, which was later on called the Eldena monastery, south of the mouth of the river Ryck. This was even confirmed by the Pope in 1204.  

After 1535, the convent was converted into a ducal office.

In 1633, Wallensteins troops destroyed the monastery to a great extent. During times of Swedish occupation (~ 1630-1815), it was used as a quarry. Only in 1827 did the Prussian King Friedrich Wilhelm IV forbid the further destruction, as he became aware of the monastery through the works of Caspar David Friedrich.  

Some parts of the monastery's complex have survived like parts of the abbey church, the eastern wing of the enclosure building, a domestic house, as well as a section of the surrounding wall.   The western wall and some pillars of the nave are still visible. The huge pointed arch window juts out between the trees like a gigantic archway and conveys an impression of the former area. From the former two-storey eastern enclosure buildings the four surrounding walls are still in existence.  

The works of Caspar David Friedrich, the great painter of German Romanticism, made the monastery ruins of Eldena world-famous and a symbol for the whole period of Romanticism. Several works of Caspar David Friedrich are exhibited in the art gallery (Gemäldegalerie) of the Pomeranian Museum Pommersches Landesmuseum).  The grounds of the monastery ruins were extensively redeveloped on the occasion of its 800th anniversary. ";


    GUIStyle st;
    void OnGUI()
    {
       
        if (locationMenu.loadLocation == "zurich")
        {
            GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height / 3), img, ScaleMode.ScaleToFit, false, 0f);
        }

        //-------------------------------------------------------------------------
        st = new GUIStyle();
        st.fontSize = (int)Mathf.Round(6 * Screen.width / 320);

        scrollViewVector = GUI.BeginScrollView(new Rect(0, Screen.height / 2 - 45, Screen.width, Screen.height / 2), scrollViewVector, new Rect(0, 0, 400, 400));


        innerText = GUI.TextArea(new Rect(0, 0, Screen.width - 10, 300), innerText, st);

        GUI.EndScrollView();
    }
}
