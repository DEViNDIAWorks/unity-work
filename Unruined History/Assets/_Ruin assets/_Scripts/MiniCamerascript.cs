﻿using UnityEngine;
using System.Collections;

public class MiniCamerascript : MonoBehaviour {

 // Use this for initialization
    Camera cm;
 void Start () 
    {
        if (Input.deviceOrientation == DeviceOrientation.LandscapeLeft || Input.deviceOrientation == DeviceOrientation.LandscapeRight || Input.deviceOrientation == DeviceOrientation.Portrait || Input.deviceOrientation == DeviceOrientation.PortraitUpsideDown)
        {
            previousorientation = Input.deviceOrientation;
        }
        else
        {
            previousorientation = DeviceOrientation.Portrait;
        }
 }
 
 // Update is called once per frame
    DeviceOrientation previousorientation;

 void Update () 
    {
        if (Input.deviceOrientation == DeviceOrientation.LandscapeLeft || Input.deviceOrientation == DeviceOrientation.LandscapeRight || Input.deviceOrientation == DeviceOrientation.Portrait || Input.deviceOrientation == DeviceOrientation.PortraitUpsideDown)
        {
            previousorientation = Input.deviceOrientation;
        }
        if (Input.deviceOrientation == DeviceOrientation.Portrait||Input.deviceOrientation==DeviceOrientation.PortraitUpsideDown || (Input.deviceOrientation == DeviceOrientation.Unknown||Input.deviceOrientation == DeviceOrientation.FaceUp ) && (previousorientation == DeviceOrientation.Portrait||previousorientation==DeviceOrientation.PortraitUpsideDown))
        {
            camera.rect = new Rect(0.7f, 0.1f, 0.5f, 0.25f);
        }
        else
        {
            camera.rect = new Rect(0.7f, 0.1f, 0.3f, 0.4f);
        }

       
 }
}