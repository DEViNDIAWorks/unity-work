﻿using UnityEngine;
using System.Collections;

public class UserOption : MonoBehaviour 
{
    public static bool isMini_Map;
    GameObject temple;
    GameObject firstPerson;
    Camera[] cm;
    public static bool showMiniMap;

    GameObject skyboxSides;
    GameObject pointer;

    private float distance;
    private float fixLat, fixLon;
    private MapNav gps;
    private double lat, lng;
	void Start () 
    {

        Screen.sleepTimeout = SleepTimeout.NeverSleep;
        temple = GameObject.Find("ZurichTemple");
        firstPerson = GameObject.Find("First Person Controller");
        skyboxSides = GameObject.Find("skyboxSides");
        pointer = GameObject.Find("3D_Pointer");

        cm = Camera.allCameras;
        showMiniMap = true;

        if (welcome.emulator == false)
        {
           // skyboxSides.SetActive(true);
            skyboxSides.transform.position = pointer.transform.position;
        }
        else
        {
            skyboxSides.SetActive(false);
        }
       
	}
    bool isAttach = true;	
	void Update ()
    {
        if (isMini_Map)
        {
            cm[0].active = true;//Character
            cm[1].active = false;//Map
            cm[2].active = true;//Mini

           
        }
        else
        {
            cm[0].active = false;//Character
            cm[1].active = true;//Map
            cm[2].active = false;//Mini

            temple.SetActive(true);         
            
        }
        //----------------------------------------------------------------------------------------------------
        if (isMini_Map)
        {
            if (showMiniMap)
            {
                cm[2].active = true;//Mini

                
            }
            else
            {
                cm[2].active = false;//Mini
            }
        }
        else {
            distance_Tg();//Check distace
        }
       
	}

    public static double distance_;
    void distance_Tg()
    {
        lat = GPS_Status.ddLat;
        lng = GPS_Status.ddLon;

       /* fixLat = 28.503961f;
        fixLon = 77.301828f; // Badhkal */


        fixLat = 54.08961484035801f;
        fixLon = 13.452498598340531f;

        distance_ = new DistanceCalculator().distance(lat, lng, fixLat, fixLon, 'K');

        if ((!welcome.isOffiline) && (!welcome.emulator))
        if (true)
        {
            if ((distance_ < 2) && (distance_ > 0.4))
            {
                print("current distance : " + distance_);
                skyboxSides.SetActive(true);
                skyboxSides.transform.position = pointer.transform.position;
            }
            else
            {
                skyboxSides.SetActive(false);
            }
        }
      //  print("update current distance : " + dis + " \n lat : " + lat + " \n lng : " + lng);
      //  disTance.text = "update current distance : " + dis + " \n lat : " + lat + " \n lng : " + lng;
    }
}
