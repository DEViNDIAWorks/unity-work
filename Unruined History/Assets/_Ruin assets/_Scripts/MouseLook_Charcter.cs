using UnityEngine;
using System.Collections;

public class MouseLook_Charcter : MonoBehaviour 
{
    
	public enum RotationAxes { MouseXAndY = 0, MouseX = 1, MouseY = 2 }
	public RotationAxes axes = RotationAxes.MouseXAndY;
	public float sensitivityX = 15F;
	public float sensitivityY = 15F;

	public float minimumX = -360F;
	public float maximumX = 360F;

	public float minimumY = -60F;
	public float maximumY = 60F;

	float rotationY = 0F;

    GameObject pointer;
    
    public static bool isRotate = true;
    
	void Update ()
	{

        //if (UserOption.isMini_Map)
        //{
        //    if (Input.GetAxis("Mouse X") > 1 || Input.GetAxis("Mouse X") < -1)
        //    {
        //        isRotate = true;
        //    }
        //}
        //else
        //{
        //    isRotate = false;
        //}
        
         
        if (isRotate)
        {

            if (axes == RotationAxes.MouseXAndY)
            {
                float rotationX = transform.localEulerAngles.y + Input.GetAxis("Mouse X") * sensitivityX;

                rotationY += Input.GetAxis("Mouse Y") * sensitivityY;
                rotationY = Mathf.Clamp(rotationY, minimumY, maximumY);

                transform.localEulerAngles = new Vector3(-rotationY, rotationX, 0);
            }
            else if (axes == RotationAxes.MouseX)
            {
                transform.Rotate(0, Input.GetAxis("Mouse X") * sensitivityX, 0);
            }
            else
            {
                rotationY += Input.GetAxis("Mouse Y") * sensitivityY;
                rotationY = Mathf.Clamp(rotationY, minimumY, maximumY);

                transform.localEulerAngles = new Vector3(-rotationY, transform.localEulerAngles.y, 0);
            }
        }
        else
        {
          //  transform.rotation = pointer.transform.rotation;
        }
	}
	
	void Start ()
	{
		// Make the rigid body not change rotation
		if (GetComponent<Rigidbody>())
			GetComponent<Rigidbody>().freezeRotation = true;

        
        pointer = GameObject.Find("3D_Pointer");
	}
}