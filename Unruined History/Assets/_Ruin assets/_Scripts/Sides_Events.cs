﻿using UnityEngine;
using System.Collections;

public class Sides_Events : MonoBehaviour
{

    private Material[] skyMat;
    void Start()
    {
        skyMat = new Material[6];
        for (int i = 0; i < skyMat.Length; i++)
        {
            skyMat[i] = (Material)Resources.Load(("Skybox_gr" + (i + 1)));

        }
    }
    // Update is called once per frame
    float timer;
    void Update()
    {
        timer = timer + Time.deltaTime;
    }
    public Texture2D image;
    bool draw = false;
    GUIStyle label;

    void OnMouseUp()
    {

        draw = true;
        //get wich object is clicked
        //Debug.Log ("clicked " + gameObject.name);
        timer = 0f;
        string obj = gameObject.name;

        switch (obj)
        {

            case "Side_1":
                {
                    RenderSettings.skybox = skyMat[0];
                    break;
                }
            case "Side_2":
                {
                    RenderSettings.skybox = skyMat[1];
                    break;
                }
            case "Side_3":
                {
                    RenderSettings.skybox = skyMat[2];
                    break;
                }
            case "Side_4":
                {
                    RenderSettings.skybox = skyMat[3];
                    break;
                }
            case "Side_5":
                {
                    RenderSettings.skybox = skyMat[4];
                    break;
                }
        }
        if (timer < 5)
            UserOption.isMini_Map = true;
    }

    void OnGUI()
    {        
        label = GUI.skin.GetStyle("Label");
        label.fontSize = (int)Mathf.Round(10 * Screen.width / 320);
        label.font = Resources.Load("Neuropol") as Font;

        if (draw)
        {
            GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), image);
            GUI.Label(new Rect((Screen.width / 2) - 130, Screen.height / 2, 260, 60), "Loading...");
          
        }
        if (UserOption.isMini_Map)
            draw = false;
    }
}
