﻿using UnityEngine;
using System.Collections;

public class CameraController3 : MonoBehaviour
{
    public GameObject player;
    private Vector3 offSet;

    public GUIStyle guiSt;
    void Start()
    {
        offSet = transform.position; //assign initial position 
        Sensor.Activate(Sensor.Type.Accelerometer);
    }

    void LateUpdate()
    {
       // transform.position = player.transform.position + offSet;
        Vector3 linearAcc = FilterMax(Sensor.accelerometer / 20 * 1 * Time.deltaTime);
        transform.Rotate(new Vector3(-linearAcc.x, rigidbody.position.y, -linearAcc.y));
    }
    Vector3 holder = Vector3.zero;
    Vector3 max = Vector3.zero;
    Vector3 velocity = Vector3.zero;

    Vector3 FilterMax(Vector3 input)
    {
        if (input.magnitude > max.magnitude) max = input;

        holder = Vector3.SmoothDamp(holder, max, ref velocity, 0.1f);
        if (Vector3.Distance(holder, max) < 0.4f) max = Vector3.zero;
        return holder;
    }
}