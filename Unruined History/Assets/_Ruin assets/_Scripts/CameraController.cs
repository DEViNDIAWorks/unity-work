﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour
{
    public GameObject player;
    private Vector3 offSet;

    void Start()
    {
        offSet = transform.position; //assign initial position 
    }

    void LateUpdate()
    {
        transform.position = player.transform.position + offSet;
       
       
    }
}