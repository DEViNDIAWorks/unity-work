﻿using UnityEngine;
using System.Collections;
using System.Threading;
using ZXing;
using ZXing.QrCode;
using System;

public class QR_Script : MonoBehaviour
{
    // Texture for encoding test
    public Texture2D encoded;

    private WebCamTexture camTexture;
    public Quaternion baseRotation;

    private Color32[] c;
    private int W, H;

    private Rect screenRect;

    public static string skyNo;
    private bool shouldEncodeNow;
    string previousorientation;
    GameObject plane;
    private float screenX;
    private float screenY;

    public Camera cam;

    public Texture2D topLeft;
    public Texture2D bottomLeft;
    public Texture2D topRight;
    public Texture2D bottomRight;

    Rect rec;
    Texture2D destTex;
    string LastResult;
    bool isQuit;


    Thread qrThread;
    int width, height;
    public GameObject pln;
    void Awake()
    {
        screenX = Screen.width;
        screenY = Screen.height;
    }

    void Start()
    {

        skyNo = "";
        shouldEncodeNow = true;

        screenRect = new Rect(0, 0, Screen.width, Screen.height);

        camTexture = new WebCamTexture();


        renderer.material.mainTexture = camTexture;
        baseRotation = transform.rotation;

        OnEnable();

        qrThread = new Thread(DecodeQR);
        qrThread.Start();

        LastResult = "";

        W = camTexture.width;
        H = camTexture.height;

        rec = new Rect(camTexture.width * .23f, camTexture.height * .3f, camTexture.width * .5f, camTexture.height * .5f);

        destTex = new Texture2D((int)rec.width, (int)rec.height);

        qrThread = new Thread(DecodeQR);
        qrThread.Start();

    }

    void Update()
    {

        transform.rotation = baseRotation * Quaternion.AngleAxis(camTexture.videoRotationAngle, Vector3.up);

        camTexture.requestedHeight = Screen.width;
        camTexture.requestedWidth = Screen.height;

        Color[] pix = camTexture.GetPixels((int)rec.x, (int)rec.y, (int)rec.width, (int)rec.height);

        destTex.SetPixels(pix);

        destTex.Apply();
        pln.renderer.material.mainTexture = destTex;
        if (c == null)
        {
            width = destTex.width;
            height = destTex.height;
            c = destTex.GetPixels32();
        }

        camTexture.requestedHeight = Convert.ToInt16(screenY);
        camTexture.requestedWidth = Convert.ToInt16(screenX);
        if (skyNo != "")
        {
            if (camTexture != null)
            {
                camTexture.Stop();
                Application.LoadLevel("scn_QRSky");
            }
        }
    }

    void DecodeQR()
    {
        // create a reader with a custom luminance source
        var barcodeReader = new BarcodeReader { AutoRotate = false, TryHarder = false };

        while (true)
        {
            if (isQuit)
                break;

            try
            {

                // decode the current frame
                var result = barcodeReader.Decode(c, width, height);
                if (result != null)
                {
                    skyNo = result.Text;

                    print(result.Text);
                }

                // Sleep a little bit and set the signal to get the next frame
                Thread.Sleep(200);
                c = null;
            }
            catch
            {
            }
        }
    }

    void OnGUI()
    {
        if (Input.deviceOrientation == DeviceOrientation.LandscapeLeft || Input.deviceOrientation == DeviceOrientation.LandscapeRight)
        {

            previousorientation = "LandscapeLeft";
        }
        if (Input.deviceOrientation == DeviceOrientation.Portrait || Input.deviceOrientation == DeviceOrientation.PortraitUpsideDown)
        {
            previousorientation = "Portrait";
        }


        screenX = Screen.width;
        screenY = Screen.height;

        GUI.skin.box.alignment = TextAnchor.MiddleCenter;
        GUI.skin.box.font = Resources.Load("Neuropol") as Font;
        GUI.skin.box.normal.background = Resources.Load("grey") as Texture2D;
        if (Screen.width >= Screen.height)
        {
            GUI.skin.button.fontSize = (int)Mathf.Round(10 * Screen.width / 480);
            GUI.skin.box.fontSize = (int)Mathf.Round(10 * Screen.width / 320);
        }
        else
        {
            GUI.skin.button.fontSize = (int)Mathf.Round(10 * Screen.height / 480);
            GUI.skin.box.fontSize = (int)Mathf.Round(10 * Screen.height / 320);
        }
        GUI.Label(new Rect(0, 0, 120, 30), skyNo);

        if (Input.deviceOrientation == DeviceOrientation.LandscapeLeft || Input.deviceOrientation == DeviceOrientation.LandscapeRight)
        {
            GUI.DrawTexture(new Rect(Screen.width * .2f, Screen.height * 0.07f, 50, 50), topLeft, ScaleMode.ScaleToFit);
            GUI.DrawTexture(new Rect(Screen.width * .7f, Screen.height * 0.07f, 50, 50), topRight, ScaleMode.ScaleToFit);
            GUI.DrawTexture(new Rect(Screen.width * .7f, Screen.height * 0.8f, 50, 50), bottomRight, ScaleMode.ScaleToFit);
            GUI.DrawTexture(new Rect(Screen.width * .2f, Screen.height * 0.8f, 50, 50), bottomLeft, ScaleMode.ScaleToFit);
            cam.fieldOfView = 60f;
        }
        else if (Input.deviceOrientation == DeviceOrientation.Portrait || Input.deviceOrientation == DeviceOrientation.PortraitUpsideDown)
        {
            GUI.DrawTexture(new Rect(Screen.width * .1f, Screen.height * 0.3f, 50, 50), topLeft, ScaleMode.ScaleToFit);
            GUI.DrawTexture(new Rect(Screen.width * .1f, Screen.height * 0.7f, 50, 50), bottomLeft, ScaleMode.ScaleToFit);
            GUI.DrawTexture(new Rect(Screen.width * .8f, Screen.height * 0.7f, 50, 50), bottomRight, ScaleMode.ScaleToFit);
            GUI.DrawTexture(new Rect(Screen.width * .8f, Screen.height * 0.3f, 50, 50), topRight, ScaleMode.ScaleToFit);

            cam.fieldOfView = 90f;
        }
        else if (previousorientation == "Portrait" && (Input.deviceOrientation == DeviceOrientation.Unknown || Input.deviceOrientation == DeviceOrientation.FaceUp || Input.deviceOrientation == DeviceOrientation.FaceDown))
        {
            GUI.DrawTexture(new Rect(Screen.width * .1f, Screen.height * 0.3f, 50, 50), topLeft, ScaleMode.ScaleToFit);
            GUI.DrawTexture(new Rect(Screen.width * .1f, Screen.height * 0.7f, 50, 50), bottomLeft, ScaleMode.ScaleToFit);
            GUI.DrawTexture(new Rect(Screen.width * .8f, Screen.height * 0.7f, 50, 50), bottomRight, ScaleMode.ScaleToFit);
            GUI.DrawTexture(new Rect(Screen.width * .8f, Screen.height * 0.3f, 50, 50), topRight, ScaleMode.ScaleToFit);

            cam.fieldOfView = 90f;
        }
        else if (previousorientation == "LandscapeLeft" && (Input.deviceOrientation == DeviceOrientation.Unknown || Input.deviceOrientation == DeviceOrientation.FaceUp || Input.deviceOrientation == DeviceOrientation.FaceDown))
        {
            GUI.DrawTexture(new Rect(Screen.width * .2f, Screen.height * 0.07f, 50, 50), topLeft, ScaleMode.ScaleToFit);
            GUI.DrawTexture(new Rect(Screen.width * .7f, Screen.height * 0.07f, 50, 50), topRight, ScaleMode.ScaleToFit);
            GUI.DrawTexture(new Rect(Screen.width * .7f, Screen.height * 0.8f, 50, 50), bottomRight, ScaleMode.ScaleToFit);
            GUI.DrawTexture(new Rect(Screen.width * .2f, Screen.height * 0.8f, 50, 50), bottomLeft, ScaleMode.ScaleToFit);
            cam.fieldOfView = 60f;
        }
        else
        {
            GUI.DrawTexture(new Rect(Screen.width * .1f, Screen.height * 0.3f, 50, 50), topLeft, ScaleMode.ScaleToFit);
            GUI.DrawTexture(new Rect(Screen.width * .1f, Screen.height * 0.7f, 50, 50), bottomLeft, ScaleMode.ScaleToFit);
            GUI.DrawTexture(new Rect(Screen.width * .8f, Screen.height * 0.7f, 50, 50), bottomRight, ScaleMode.ScaleToFit);
            GUI.DrawTexture(new Rect(Screen.width * .8f, Screen.height * 0.3f, 50, 50), topRight, ScaleMode.ScaleToFit);

            //cam.fieldOfView = 90f;
        }

        if (GUI.Button(new Rect(3 * screenX / 5, screenY - screenY / 12, screenX / 5, screenY / 12), "Restart"))
        {

            try
            {
                camTexture.Stop();
            }
            catch (Exception exx) { }
            Application.LoadLevel("scn_Welcome");
        }

        //Show GPS Status info. Please make sure the GPS_Status.cs script is attached and enabled in the map object.
        if (GUI.Button(new Rect(4 * screenX / 5, screenY - screenY / 12, screenX / 5, screenY / 12), "Quit"))
        {
            camTexture.Stop();
            Application.Quit();
        }

        if (Input.GetKey(KeyCode.Space))
        {
            cam.fieldOfView = 160f;
            print("space press");
        }

    }

    void OnEnable()
    {
        if (camTexture != null)
        {
            camTexture.Play();
            W = camTexture.width;
            H = camTexture.height;
        }
    }

    void OnDisable()
    {
        if (camTexture != null)
        {
            //camTexture.Pause();
            camTexture.Stop();
        }
    }

    void OnDestroy()
    {
        try
        {
            qrThread.Abort();
            camTexture.Stop();
        }
        catch (Exception excp)
        {

        }
    }

    // It's better to stop the thread by itself rather than abort it.
    void OnApplicationQuit()
    {
        isQuit = true;
    }


}
