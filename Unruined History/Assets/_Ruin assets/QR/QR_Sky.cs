﻿using UnityEngine;
using System.Collections;

public class QR_Sky : MonoBehaviour {

    private float screenX;
    private float screenY;

    Material skybox1;
    Material skybox2;
    Material skybox3;
    Material skybox4;
    Material skybox5;
    string Code;

    void Awake()
    {
        screenX = Screen.width;
        screenY = Screen.height;
    }
	void Start ()
    {
        skybox1 = (Material)Resources.Load("Skybox_gr1");
        skybox2 = (Material)Resources.Load("Skybox_gr2");
        skybox3 = (Material)Resources.Load("Skybox_gr3");
        skybox4 = (Material)Resources.Load("Skybox_gr4");
        skybox5 = (Material)Resources.Load("Skybox_gr5");
        Code = QR_Script.skyNo;
       
        
	}
	
	// Update is called once per frame
	void Update ()
    {
	 
	}

    void OnGUI()
    {
        screenX = Screen.width;
        screenY = Screen.height;

        GUI.skin.box.alignment = TextAnchor.MiddleCenter;
        GUI.skin.box.font = Resources.Load("Neuropol") as Font;
        GUI.skin.box.normal.background = Resources.Load("grey") as Texture2D;
        if (Screen.width >= Screen.height)
        {
            GUI.skin.button.fontSize = (int)Mathf.Round(10 * Screen.width / 480);
            GUI.skin.box.fontSize = (int)Mathf.Round(10 * Screen.width / 320);
        }
        else
        {
            GUI.skin.button.fontSize = (int)Mathf.Round(10 * Screen.height / 480);
            GUI.skin.box.fontSize = (int)Mathf.Round(10 * Screen.height / 320);
        }
        if (Code == "Sky1")
        {
            SceneChange(skybox1);
        }
        else if (Code == "Sky2")
        {
            SceneChange(skybox2);
        }
        else if (Code == "Sky3")
        {
            SceneChange(skybox3);
        }
        else if (Code == "Sky4")
        {
            SceneChange(skybox4);
        }
        else if (Code == "Sky5")
        {
            SceneChange(skybox5);
        }
        
        if (GUI.Button(new Rect(10, 20, screenX / 5, screenY / 12), "BACK"))
        {
           // RenderSettings.skybox = null;
            Application.LoadLevel("scn_QR");
        }
        if (GUI.Button(new Rect(3 * screenX / 5, screenY - screenY / 12, screenX / 5, screenY / 12), "Restart"))
        {

            Application.LoadLevel("scn_Welcome");
        }

        //Show GPS Status info. Please make sure the GPS_Status.cs script is attached and enabled in the map object.
        if (GUI.Button(new Rect(4 * screenX / 5, screenY - screenY / 12, screenX / 5, screenY / 12), "Quit"))
        {
            
            Application.Quit();
        }
    }

    void SceneChange(Material sky)
    {
        print(QR_Script.skyNo);
        RenderSettings.skybox = sky;
    }
    
}
